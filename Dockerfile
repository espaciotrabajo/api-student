FROM openjdk:11.0.6-jdk
LABEL maintainer="developer@mitocode.com"
WORKDIR /workspace
RUN ls -la /workspace
COPY /target/api*.jar app.jar
RUN ls -la /workspace
EXPOSE 8912
ENTRYPOINT exec java -jar /workspace/app.jar